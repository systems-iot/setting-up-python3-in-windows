# Setting Up Python3 in Windows

For an in-depth walk-through, review the video.

## Steps/Links

1. Download Python [link](https://www.python.org/ftp/python/3.7.4/python-3.7.4.exe)
2. Install Python - Make sure to check the `Set Python on Path` in the first installation screen.
3. Verify the python installation by opening a command window and typing `python`

> Troubleshooting: If python cannot be found, be sure to watch the end of the video to set your Windows path
